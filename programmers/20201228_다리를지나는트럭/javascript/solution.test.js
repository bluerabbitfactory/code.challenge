const solution = require('./solution.js')

const TestCases = [
  { bLenghth: 2, weight: 10, truck_w: [7, 4, 5, 6], answer: 8 },
  { bLenghth: 100, weight: 100, truck_w: [10], answer: 101 },
  { bLenghth: 100, weight: 100, truck_w: [10,10,10,10,10,10,10,10,10,10], answer: 110 }
]

for(const tCase of TestCases) {
  test(`다리를 지나는 트럭: ${tCase.answer}`, () => { expect( solution(tCase.bLenghth, tCase.weight, tCase.truck_w) ).toEqual( tCase.answer) })
}
