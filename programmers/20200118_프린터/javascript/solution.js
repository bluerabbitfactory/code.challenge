/**
 * 프린터
 * https://programmers.co.kr/learn/courses/30/lessons/42587
 * wh.lee@bfactory.inc
 **/

module.exports = solution = (priorities = [], location) => { 
  var answer = 0
  // add Tag
  Array.prototype.addTag = function (pos){
    this.forEach((val, idx)=> this[idx] = [val,idx === pos? true : false])
  }
  // do printing
  Array.prototype.nextPrint = function (){
    let val = this.shift()
    while(this.some((v)=> v[0] > val[0])) {
      this.push(val)
      val = this.shift()
    }
    return val 
  }
  // ----------------------------------------
  // logic
  priorities.addTag(location)
  let hasNext = true
  while(hasNext) {
    answer += 1
    if(priorities.nextPrint()[1]) {
      hasNext = false
    }
  }
  return answer
}

