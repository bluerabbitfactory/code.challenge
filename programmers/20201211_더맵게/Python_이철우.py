import heapq


# def solution(scoville, K):
#     answer = 0
#     data = []
#
#     for s in scoville:
#         data.append(s)
#
#     while len(data) > 0:
#         data.sort()
#         if data[0] >= K:
#             return answer
#         x = data.pop(0)
#         if data:
#             y = data.pop(0)
#             data.insert(0, x + (y * 2))
#         answer += 1
#
#     return -1

def solution(scoville, K):
    answer = 0
    data = []

    for s in scoville:
        heapq.heappush(data, s)

    while len(data) > 0:
        if data[0] >= K:
            return answer
        x = heapq.heappop(data)
        if data:
            y = heapq.heappop(data)
            heapq.heappush(data, x + (y * 2))
        answer += 1

    return -1


print(solution([1, 2, 3, 9, 10, 12], 7))  # ---> 2

a = [4, 3, 2, 1, 5]
print(a)  # ---> 4, 3, 2, 1, 5

heapq.heapify(a)
print(a)  # ---> 1, 3, 2, 4, 5

x = heapq.heappop(a)  # ---> 1
print(x)  # ---> 1
print(a)  # ---> 2, 3, 5, 4
