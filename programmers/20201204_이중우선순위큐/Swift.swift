//
//  Swift.swift
//  
//
//  Created by Aaron Lee on 2020/12/04.
//

import Foundation

func solution(_ operations:[String]) -> [Int] {
    
    var queue = [Int]()
    
    for operation in operations {
        
        // OPERATOR
        let myOperator = Operator.allCases.filter { operation.hasPrefix($0.rawValue) }.first
        guard let safeOperator = myOperator else { fatalError("No Valid Operator in Operation: \(operation)") }
        
        switch safeOperator {
        
        // INSERT
        case .insert:
            let values = operation.split(separator: " ")
            guard let value = Int(values[values.count - 1]) else { continue }
            queue.append(value)
            
        // DELETE MAX
        case .deleteMax:
            queue = queue.filter { $0 != queue.max() }
            
        // DELETE MIN
        case .deleteMinimum:
            queue = queue.filter { $0 != queue.min() }
        }
        
    }
    
    // isEmpty ? [0, 0] : [max, min]
    return [queue.max() ?? 0, queue.min() ?? 0]
}

/// Definition of Delete Operators
enum Operator: String ,CaseIterable {
    case insert = "I"
    case deleteMax = "D 1"
    case deleteMinimum = "D -1"
}

print(solution(["I 16","D 1"])) // ---> [0, 0]
print(solution(["I 7","I 5","I -5","D -1"])) // ---> [7, 5]
print(solution(["I 16", "I -5643", "D -1", "D 1", "D 1", "I 123", "D -1"])) // ---> [0, 0]
print(solution(["I -45", "I 653", "D 1", "I -642", "I 45", "I 97", "D 1", "D -1", "I 333"])) // ---> [333, -45]
