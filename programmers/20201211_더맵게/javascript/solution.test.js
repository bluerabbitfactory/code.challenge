const solution = require('./solution.js')

const TestCases = [
  { scoville: [1, 2, 3, 9, 10, 12], k: 7, result: 2 }
]

test('최소 스코빌지수 만들기: ', () => {
  expect( 
    solution(TestCases[0].scoville, TestCases[0].k)
  ).toBe(
    TestCases[0].result
  )
})
