const solution = require('./solution.js')
const heapSort = require('./heapSort.js')

const TestCases = [
  { operations: ["I 16","D 1"], result: [0,0] },
  { operations: ["I 7","I 5","I -5","D -1"], result: [7,5] },
  { operations: ["I -45", "I 653", "D 1", "I -642", "I 45", "I 97", "D 1", "D -1", "I 333"], result: [333, -45] }
]

test("heapSort 테스트 ", () => {  
  expect( 
    heapSort([2,5,1,6,3,10,4,7,9,8]) 
  ).toEqual(
    [1,2,3,4,5,6,7,8,9,10]
  )
})

test("이중 우선순위 큐 처리 결과 T1 ", () => {  
  expect( 
    solution(TestCases[0].operations) 
  ).toEqual(
    TestCases[0].result
  )
})


test("이중 우선순위 큐 처리 결과 T2 ", () => {  
  expect( 
    solution(TestCases[1].operations) 
  ).toEqual(
    TestCases[1].result
  )
})


test("이중 우선순위 큐 처리 결과 T3 ", () => {  
    expect( 
      solution(TestCases[2].operations) 
    ).toEqual(
      TestCases[2].result
    )
})


