/**
 * 기능개발
 * https://programmers.co.kr/learn/courses/30/lessons/42586
 * wh.lee@bfactory.inc
 **/
module.exports = solution = (progresses = [], speeds = []) => {
  const answer = []
  const calcTasks = progresses.map((el, idx) => Math.ceil((100-el) / speeds[idx]))
  let F = calcTasks[0], tmpS = 0, count = 1, taskLength = calcTasks.length
  for(let idx = 1; idx < taskLength; idx++) {
    tmpS = calcTasks[idx]
    if(F < tmpS){
      answer.push(count)
      F = tmpS
      count = 1
    } else {
      count += 1
    }
  }
  answer.push(count)
  /**
   * type 1
   * 
      let count = 1
      let F = calcTasks.shift() 
      while((tmpS = calcTasks.shift()) !== undefined) {
        if(F < tmpS){
          answer.push(count)
          F = tmpS
          count = 1
        } else {
          count += 1
        }
      }
      answer.push(count)
  */
  return answer
}
// solution([93, 30, 55], [1, 30, 5])
// solution([95, 90, 99, 99, 80, 99], [11, 1, 1, 1, 1, 1])
// solution([95, 90, 99, 99, 80, 99, 5, 2], [1, 1, 1, 1, 1, 1, 20, 30])