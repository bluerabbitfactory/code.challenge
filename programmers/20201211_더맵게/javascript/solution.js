/**
 * 더 맵게
 * wh.lee@bfactory.inc
 **/
module.exports = solution = (scoville, k) => {
  let loop = 0

  Array.prototype.mixScoville = function () {
    const calcMixScoville = (mF, mS) => mF + (mS * 2)
    const nSHU = calcMixScoville(this[0],this[1])
    this.shift() 
    this[0] = nSHU
    return this.sort((a,b)=>a-b)
  }
  // ---------------------------------------
  scoville.sort((a,b)=>a-b)
  // ---------------------------------------
  while(true) {
    if(scoville[0] > k) {
      break;
    }
    scoville.mixScoville()
    loop++;
  }
  console.log(`loop: ${loop}`)
  return loop
}