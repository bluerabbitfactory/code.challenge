const solution = require('./solution.js')

const TestCases = [
  { progresses: [93, 30, 55], speeds: [1, 30, 5], result: [2,1] },
  { progresses: [95, 90, 99, 99, 80, 99], speeds: [1, 1, 1, 1, 1, 1], result: [1, 3, 2 ]},
  { progresses: [95, 90, 99, 99, 80, 99, 5, 2], speeds: [1, 1, 1, 1, 1, 1, 20, 30], result: [1, 3, 4 ]}
]

for(const tCase of TestCases) {
  test(`기능개발: ${tCase.progresses}`, () => { expect( solution(tCase.progresses, tCase.speeds) ).toEqual( tCase.result) })
}


