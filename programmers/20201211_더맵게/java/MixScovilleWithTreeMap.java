/**
 * 더 맵
 * wh.lee@bfactory.inc
 *
 * 2020.12.11 - wh.lee
 * - 테스트케이스 12번에서 오류 발생 함
 * - 효율성 통과 못함
 */

package algorithm.programers;

import java.util.Map.Entry;
import java.util.TreeMap;

public class MixScovilleWithTreeMap {

  public int solution(int[] scoville, int K) {
    int loop = 0, idx = 0, jdx = 0;
    int[] values = new int[2];
    Boolean isMix = true;
    TreeMap<Integer, Integer> treeMap = new TreeMap<>((a,b)->a-b);

    for(idx = 0; idx < scoville.length; idx ++) {
      treeMap.put(Integer.valueOf(scoville[idx]), Integer.valueOf(scoville[idx]));
    }
    // --------------------------
    while(true) {
      jdx = 0;
      if(treeMap.firstKey() >= K) { break; }
      if(treeMap.size() < 2) {isMix = false; break;}
      // ----------------------
      for (Entry<Integer, Integer> entry : treeMap.entrySet()) {
        if(jdx > 1){
          break;
        }
        values[jdx] = entry.getKey();
        jdx++;
      }
      System.out.println(values[0] + "/" + values[1]);
      Integer nSHU = values[0] + (values[1] * 2);
      treeMap.remove(values[0]);
      treeMap.remove(values[1]);
      treeMap.put(nSHU, nSHU);
      loop++;
    }
    return isMix ? loop : -1;
  }
}

