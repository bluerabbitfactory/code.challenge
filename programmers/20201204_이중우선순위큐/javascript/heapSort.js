module.exports = heapSort = (target) => {
  // --
  const heapify = (target, sIdx) => {
    for(let idx = 1; idx < sIdx; idx++) {
      let pos = idx;
      while(pos > 0) { 
        const pIdx = Math.floor((pos-1) /2)
        if(target[pos] > target[pIdx]) {
          swap(target, pos, pIdx)
        }
        pos = pIdx
      }
    }
  }
  // --
  const swap = (target, sIdx, eIdx) => {
    const temp = target[sIdx]
    target[sIdx] = target[eIdx]
    target[eIdx] = temp
    return target
  }
  // start heap sort
  heapify(target, target.length)
  for(let idx = target.length-1; idx > 0 ; idx-- ) {
    swap(target, 0, idx);
    heapify(target, idx);
  }
  // for testcase
  return target
}