const solution = require('./solution.js')

const TestCases = [
  { priorities: [2, 1, 3, 2], location: 2, return: 1 },
  { priorities: [1, 1, 9, 1, 1, 1], location: 0, return: 5 }
]

for(const tCase of TestCases) {
  test(`프린터: ${tCase.return}`, () => { expect( solution(tCase.priorities, tCase.location) ).toEqual( tCase.return) })
}