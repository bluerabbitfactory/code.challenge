/**
 * 다리를 지나는 트럭
 * https://programmers.co.kr/learn/courses/30/lessons/42583
 * wh.lee@bfactory.inc
 **/
class CrossBrigeQueue {
  bL = 0; bW = 0; onbW = 0; bQ = undefined
  constructor (leng, weight) {
    this.bL = leng
    this.bW = weight
    this.bQ = new Array(this.bL).fill(0)
  }
  isNext(truck) {
    this.crossTruck()
    if(this.bW >= (this.onbW + truck)) {
      this.bQ.push(truck)
      this.onbW += truck
      return true
    }
    return false
  }
  crossTruck() {
    this.onbW -= this.bQ[0]
    this.bQ.shift()
    if(this.bQ.length !== this.bL-1) {
      this.bQ.push(0)
    }
  }
  isEmpty() {
    return this.onbW === 0
  }
}
module.exports = solution = (bridge_length, weight, truck_weights = []) => {
  let sec = 1, idx = 0
  const crossBridge = new CrossBrigeQueue(bridge_length, weight)
  while(true) {
    if(crossBridge.isNext(truck_weights[idx] || 0)) {
      idx++;
    }
    if(crossBridge.isEmpty()) {
      break;
    }
    sec++;  
  }
  // console.log(`Sec: ${sec}`)
  return sec
}
